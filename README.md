# HEAT MAP #
Uses the deck.gl library to display a heatmap based on the amount of coordinates in the same point in the map.

![Screenshot](print.png)
### How do I get set up? ###

* npm install
* create a .env file based in the .env.example
* npm start

### MAPBOX ###
* API key required to have the map loaded.

### Datasets ###
* json_array_july.json
* json_array.json (since fev/2021)
until 26/nov/2021